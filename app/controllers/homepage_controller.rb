require 'base64'
require 'net/http'

class HomepageController < ApplicationController
  def index
  end

  def create
    file = params['file']
    params['data'] = Base64.encode64(file.read)
    params['filename'] = file.original_filename

    url = URI.parse('http://localhost:4567/upload')
    req = Net::HTTP::Post.new(url.to_s)
    req.set_form_data(params.except('file'))

    res = Net::HTTP.start(url.hostname, url.port) do |http|
      http.request(req) rescue nil
    end

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
    else
    end

    redirect_to homepage_index_path
  end
end
