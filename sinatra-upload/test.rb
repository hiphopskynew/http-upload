require 'sinatra'
require 'sinatra/cross_origin'
require 'base64'
require 'json'

configure do
  enable :cross_origin
end

get '/hi' do
  "Hello World!"
end

post '/upload' do
  begin
    data = params["data"]
    filename = params["filename"]
    title  = params["title"]

    path = "./uploads/#{filename}"
    File.delete(path) rescue ''
    File.open(path, 'wb') do |f|
      f.write(Base64.decode64(data))
    end

    "Write file successful."
  rescue
    "Write file errors."
  end
end
